//package org.openmarkov.inference.variableElimination.action;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import org.openmarkov.core.action.AddPotentialEdit;
//import org.openmarkov.core.action.PNEdit;
//import org.openmarkov.core.action.RemoveNodeEdit;
//import org.openmarkov.core.action.RemovePotentialEdit;
//import org.openmarkov.core.action.UsesVariable;
//import org.openmarkov.core.exception.NonProjectablePotentialException;
//import org.openmarkov.core.exception.PotentialOperationException;
//import org.openmarkov.core.model.network.Node;
//import org.openmarkov.core.model.network.ProbNet;
//import org.openmarkov.core.model.network.Variable;
//import org.openmarkov.core.model.network.potential.GTablePotential;
//import org.openmarkov.core.model.network.potential.Potential;
//import org.openmarkov.core.model.network.potential.PotentialRole;
//import org.openmarkov.core.model.network.potential.TablePotential;
//import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
//import org.openmarkov.inference.variableElimination.operation.CEPotentialOperation;
//
//@SuppressWarnings("serial")
//public class CRemoveBCDecisionVariable extends CRemoveBCVariableEdit
//		implements PNEdit, UsesVariable  {
//
//	// Attributes
//	private Map<Variable, TablePotential> optimalStrategy;
//
//	// Constructor
//	/**
//	 * @param probNet. <code>ProbNet</code>
//	 * @param variableToDelete. <code>Variable</code>
//	 * @param optimalStrategy. <code>Map key = Variable, value = GTablePotential of CEPartition</code>
//	 */
//	public CRemoveBCDecisionVariable(
//			ProbNet probNet,
//			Variable decisionVariable,
//			Map<Variable, TablePotential> optimalStrategy) {
//		super(probNet, decisionVariable);
//		this.optimalStrategy = optimalStrategy;
//	}
//
//	// Methods
//	@Override
//	public void generateEdits() throws NonProjectablePotentialException {
//		// 1 Gets all the probability potentials that depends on variable (=variableToDelete).
//		List<Potential> probPotentials = probNet.getProbPotentials(variable);
//		// 2 Gets all the cost-effectiveness potentials that depends on variable. There is one and only one.
//		GTablePotential oldCEPotential = (GTablePotential)probNet.getUtilityPotentials(variable).get(0);
//
//		// 3 Remove Potentials from the graph
//		Node nodeToDelete = probNet.getNode(variable);
//		for (Potential probPotential : probPotentials) {
//			// 3.1 Remove probability potentials
//			this.addEdit(new RemovePotentialEdit(probNet, probPotential));
//		}
//		// 3.2 Remove utility (CE) potential
//		this.addEdit(new RemovePotentialEdit(probNet, oldCEPotential));
//
//		// 4 Remove Node from the graph
//		this.addEdit(new RemoveNodeEdit(probNet, nodeToDelete));
//
//		// 5 Convert probPotentials list (type Potential) to a list of TablePotential
//		List<TablePotential> probTablePotentials = new ArrayList<TablePotential>(probPotentials.size());
//		for (Potential probPotential : probPotentials) {
//			probTablePotentials.add((TablePotential)probPotential);
//		}
//
//		// 6 Computes the new probability potentials
//		// 6.1 Join probability
//		TablePotential joinProb = null;
//		if (probTablePotentials.size() > 0) {
//			joinProb = DiscretePotentialOperations.multiply(probTablePotentials);
//		}
//
//		// 6.2 Maximized probability
//		GTablePotential newCEPotential = null;
//		TablePotential maximizedProbPotential = null;
//		if (joinProb != null) {
//			List<Variable> variablesToKeep = joinProb.getVariables();
//			variablesToKeep.remove(variable);
//			List<Variable> variablesToEliminate = new ArrayList<Variable>(1);
//			variablesToEliminate.add(variable);
//			List<Potential> joinProbPotentials = new ArrayList<Potential>(1);
//			joinProbPotentials.add(joinProb);
//			maximizedProbPotential = (TablePotential) DiscretePotentialOperations
//				.multiplyAndMaximize(joinProbPotentials, variablesToKeep, variable)[0];
//			// 6.3 Normalized probability from maximized probability and join probability
//			TablePotential normalizedProb =	(TablePotential) DiscretePotentialOperations
//				.divide(joinProb, maximizedProbPotential);
//			// 6.4 Computes the new Cost-Effectiveness potential from normalized probability and old cePotential
//			List<Potential> probAndUtility = new ArrayList<Potential>();
//			probAndUtility.add(normalizedProb);
//			probAndUtility.add(oldCEPotential);
////			newCEPotential = CEPotentialOperation.multiply(oldCEPotential, normalizedProb);
//		}
//		// 6.5 Maximizes the cost-effectiviness potential
//		try {
//			if (newCEPotential != null) {
//				newCEPotential = CEPotentialOperation.ceMaximize(newCEPotential, variable);
//			} else {
//				newCEPotential = CEPotentialOperation.ceMaximize(oldCEPotential, variable);
//			}
//		} catch (PotentialOperationException e) {
//			throw new NonProjectablePotentialException(e.getMessage());
//		}
//		newCEPotential.setPotentialRole(PotentialRole.UTILITY);
//		newCEPotential.setUtilityVariable(oldCEPotential.getUtilityVariable());
//
//		// 7. Update optimal strategy
//		optimalStrategy.put(variable, newCEPotential);
//		// 8. Adds the new potentials to the network
//		if (maximizedProbPotential != null) {
//			this.addEdit(new AddPotentialEdit(probNet, maximizedProbPotential));
//		}
//		this.addEdit(new AddPotentialEdit(probNet, newCEPotential));
//	}
//
//}
