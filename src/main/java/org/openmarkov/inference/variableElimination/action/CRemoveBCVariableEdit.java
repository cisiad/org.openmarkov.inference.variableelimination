//package org.openmarkov.inference.variableElimination.action;
//
//import org.openmarkov.core.exception.NonProjectablePotentialException;
//import org.openmarkov.core.exception.WrongCriterionException;
//import org.openmarkov.core.model.network.ProbNet;
//import org.openmarkov.core.model.network.Variable;
//
///**
// * @author Manuel Arias
// * Base class to remove chance and decision variables in a cost-effectiveness network.
// */
//@SuppressWarnings("serial")
//public class CRemoveBCVariableEdit extends CRemoveVariableEdit {
//
//	// Constructor
//	/**
//	 * @param probNet
//	 * @param variable
//	 */
//	public CRemoveBCVariableEdit(ProbNet probNet, Variable variable) {
//		super(probNet, variable);
//	}
//
//	// Methods
//	@Override
//	public void generateEdits() throws NonProjectablePotentialException, WrongCriterionException {
//	}
//
//}
