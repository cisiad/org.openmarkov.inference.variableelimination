//package org.openmarkov.inference.variableElimination.action;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.openmarkov.core.action.AddPotentialEdit;
//import org.openmarkov.core.action.PNEdit;
//import org.openmarkov.core.action.RemoveNodeEdit;
//import org.openmarkov.core.action.RemovePotentialEdit;
//import org.openmarkov.core.action.UsesVariable;
//import org.openmarkov.core.exception.DoEditException;
//import org.openmarkov.core.exception.NonProjectablePotentialException;
//import org.openmarkov.core.exception.PotentialOperationException;
//import org.openmarkov.core.exception.WrongCriterionException;
//import org.openmarkov.core.model.network.Node;
//import org.openmarkov.core.model.network.ProbNet;
//import org.openmarkov.core.model.network.Variable;
//import org.openmarkov.core.model.network.potential.GTablePotential;
//import org.openmarkov.core.model.network.potential.Potential;
//import org.openmarkov.core.model.network.potential.PotentialRole;
//import org.openmarkov.core.model.network.potential.TablePotential;
//import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
//import org.openmarkov.core.model.network.CEP;
//import org.openmarkov.inference.variableElimination.operation.CEPotentialOperation;
//
//@SuppressWarnings("serial")
//public class CRemoveBCChanceVariable  extends CRemoveBCVariableEdit implements PNEdit, UsesVariable {
//
//	public CRemoveBCChanceVariable(ProbNet probNet, Variable variable) {
//		super(probNet, variable);
//	}
//
//	@Override
//	public void generateEdits() throws NonProjectablePotentialException, WrongCriterionException {
//		// 1 Gets all the potentials that depends on variableToDelete.
//		List<Potential> probPotentialsVariable = probNet.getProbPotentials(variable);
//
//		// 2 Remove the variable from the graph:
//		// 2.1 Remove potentials with variable
//		Node nodeToDelete = probNet.getNode(variable);
//		for (Potential probPotential : probPotentialsVariable) {
//			addEdit(new RemovePotentialEdit(probNet, probPotential));
//		}
//		// 2.2 Remove node of variable
//		addEdit(new RemoveNodeEdit(probNet, nodeToDelete));
//
//		// 3 Get the marginal probability
//		// 3.1 Computation
//		TablePotential joinProb = DiscretePotentialOperations.multiply(getTablePotentialList(probPotentialsVariable));
//		joinProb.setPotentialRole(PotentialRole.JOINT_PROBABILITY);
//
//		TablePotential marginalProb = joinProb.getNumVariables() > 0 ?
//					DiscretePotentialOperations.marginalize(joinProb, variable)
//				:
//					joinProb;
//
//		// 3.2 Add the new marginal probability to the probNet
//		this.addEdit(new AddPotentialEdit(probNet, marginalProb));
//
//		// 4 Get new utility
//		List<Potential> utilityPotentials = probNet.getUtilityPotentials(variable);
//		if (!utilityPotentials.isEmpty()) { // There are utilities
//			GTablePotential utilityPotential = (GTablePotential)utilityPotentials.get(0);
//
//			if (utilityPotential.getTableSize() > 1) {
//				addEdit(new RemovePotentialEdit(probNet, utilityPotential));
//
//				// 4.1 Computes the new utility potential
//				GTablePotential newUtilityPotential = null;
//				try {
////					newUtilityPotential = CEPotentialOperation.multiplyAndMarginalize(
////							utilityPotential, joinProb, variable);
//					if (marginalProb.getNumVariables() > 0) {
//						newUtilityPotential = CEPotentialOperation.divide(newUtilityPotential, marginalProb);
//					}
//				} catch (Exception e) {
//					throw new NonProjectablePotentialException(e.getMessage(), e.getCause());
//				}
//				newUtilityPotential.setPotentialRole(PotentialRole.UTILITY);
//				newUtilityPotential.setUtilityVariable(utilityPotential.getUtilityVariable());
//
//				if (newUtilityPotential.getTableSize() == 1) { // No more variables
//					globalCEP = (CEP)newUtilityPotential.elementTable.get(0);
//				}
//				// 4.2 Stores new utility
//				this.addEdit(new AddPotentialEdit(probNet, newUtilityPotential));
//			}
//		}
//	}
//
//	/**
//	 * Get the table potentials. If some potentials are not TablePotentials, get their CPT.
//	 * @param probPotentials
//	 * @return List of probPotentials. <code>List</code> of <code>Potential</code>
//	 * @throws DoEditException
//	 * @throws PotentialOperationException
//	 * @throws WrongCriterionException
//	 * @throws NonProjectablePotentialException
//	 */
//	protected List<TablePotential> getTablePotentialList(List<Potential> probPotentials)
//			throws NonProjectablePotentialException, WrongCriterionException {
//		List<TablePotential> tablePotentialList = new ArrayList<TablePotential>(probPotentials.size());
//		for (Potential potential : probPotentials) {
//			if (potential instanceof TablePotential) {
//				tablePotentialList.add((TablePotential)potential);
//			} else {
//				tablePotentialList.add(potential.getCPT());
//			}
//		}
//		return tablePotentialList;
//	}
//
//
//}
