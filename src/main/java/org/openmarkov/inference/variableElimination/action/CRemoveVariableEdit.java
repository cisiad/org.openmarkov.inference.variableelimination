//package org.openmarkov.inference.variableElimination.action;
//
//import org.openmarkov.core.action.CompoundPNEdit;
//import org.openmarkov.core.exception.NonProjectablePotentialException;
//import org.openmarkov.core.exception.WrongCriterionException;
//import org.openmarkov.core.model.network.NodeType;
//import org.openmarkov.core.model.network.ProbNet;
//import org.openmarkov.core.model.network.Variable;
//import org.openmarkov.core.model.network.potential.TablePotential;
//import org.openmarkov.core.model.network.CEP;
//
///** @author Manuel Arias */
//public class CRemoveVariableEdit extends CompoundPNEdit {
//
//	// Attributes
//	private static final long serialVersionUID = 5906073732163528970L;
//
//	protected Variable variable;
//
//    protected NodeType nodeType;
//
//    protected CEP globalCEP;
//
//    protected TablePotential policy;
//
//    // Constructor
//    /**
//     * @param probNet. <code>ProbNet</code>
//     * @param variable. <code>Variable</code>
//     */
//    public CRemoveVariableEdit(ProbNet probNet, Variable variable) {
//		super(probNet);
//		this.variable = variable;
//		this.nodeType = probNet.getNode(variable).getNodeType();
//	}
//
//	@Override
//	public void generateEdits() throws NonProjectablePotentialException,
//			WrongCriterionException{
//	}
//
//	/**
//	 * @return <code>CEPartition</code>
//	 */
//	public CEP getGlobalCEP() {
//		return globalCEP;
//	}
//
//	public Variable getVariable() {
//		return variable;
//	}
//
//	public TablePotential getPolicy() {
//		return policy;
//	}
//
//	public String toString() {
//		return variable.toString();
//	}
//
//}
