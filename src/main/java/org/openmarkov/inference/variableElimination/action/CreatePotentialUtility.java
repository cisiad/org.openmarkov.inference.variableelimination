package org.openmarkov.inference.variableElimination.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.openmarkov.core.exception.CostEffectivenessException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.exception.NonProjectablePotentialException;
import org.openmarkov.core.exception.PotentialOperationException;
import org.openmarkov.core.exception.WrongCriterionException;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.VariableType;
import org.openmarkov.core.model.network.potential.GTablePotential;
import org.openmarkov.core.model.network.potential.Intervention;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.PotentialRole;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.core.model.network.CEP;
import org.openmarkov.inference.variableElimination.operation.CEBaseOperations;

/** 
 * Given an influence diagram with several cost and effectiveness nodes, creates a new influence diagram 
 * with a single utility node, which has an associated cost-effectiveness potential.
 * @author Manuel Arias
 */
public class CreatePotentialUtility {
	
	public final static String defaultCEVariableName = "CostEffectiveness";

	/** Remove cost and effectiveness nodes and potentials replacing them for
	 * a new cost-effectiveness node and potential. The new 
	 * <code>Node</code> has as parents the parents of all the cost and
	 * effectiveness parents. This method assumes that there are no super value
	 * nodes 
	 * @throws PotentialOperationException 
	 * @throws CostEffectivenessException */
	public static ProbNet getInitializedID(ProbNet influenceDiagram, double lambdaMin, double lambdaMax) 
			throws CostEffectivenessException {
		// Gets cost and effectiveness potentials of id
		TablePotential costPotential = getCostPotential(influenceDiagram);
		TablePotential effectivenessPotential = getEffectivenessPotential(influenceDiagram);
		GTablePotential utilityPotential = createCEPotential(costPotential, effectivenessPotential, lambdaMin, lambdaMax);

		// Remove cost and effectiveness potentials and adds the GTablePotential
		List<Node> utilityNodes = influenceDiagram.getNodes(NodeType.UTILITY);

		// Get parents of cost and effectiveness potentials
		HashSet<Node> parents = new HashSet<Node>();
		for (Node utilityNode : utilityNodes) {
			parents.addAll(utilityNode.getParents());
		}

		// Remove cost and effectiveness potentials
		for (Node node : utilityNodes) {
			influenceDiagram.removePotentials(node.getPotentials());
		}

		// Remove cost and effectiveness nodes
		for (Node node : utilityNodes) {
			influenceDiagram.removeNode(node);
		}

		// Create ceNode
		Variable ceVariable = utilityPotential.getUtilityVariable();
		Node ceNode = influenceDiagram.addNode(ceVariable, NodeType.UTILITY);
		utilityPotential.setUtilityVariable(ceVariable);
		ceNode.addPotential(utilityPotential);

		// Add links from parents to the new node
		for (Node parent : parents) {
			try {
				influenceDiagram.addLink(parent.getVariable(), ceVariable, false);
			} catch (NodeNotFoundException e) {
				throw new CostEffectivenessException(e.getMessage());
			}
		}
		return influenceDiagram;
	}
	
    /** Creates a <code>GeneralizedTablePotential</code> of 
     *   <code>CEPartition</code> using the cost and effectiveness potentials. 
     * @param costPotential <code>TablePotential</code>.
     * @param effectivenessPotential <code>TablePotential</code>.
     * @param lambdaMin used in <code>PartitionLCE</code>.
     * @param lambdaMax used in <code>PartitionLCE</code>.
     * @throws CostEffectivenessException 
     * @throws <code>NotEnoughMemoryException</code>.
     * @argCondition <code>fsVariables</code> must be equal to 
     *   <code>effectiveness.getVariables()</code> union 
     *   <code>cost.getVariables()</code>. */
    @SuppressWarnings("unchecked")
	public static GTablePotential createCEPotential(
			TablePotential costPotential, 
			TablePotential effectivenessPotential,
			double lambdaMin, 
			double lambdaMax) 
					throws CostEffectivenessException {

    	// Gets the union of the variables of cost and effectiveness potential
    	ArrayList<Variable> ceVariables = new ArrayList<Variable>(costPotential.getVariables());
    	
    	ArrayList<Variable> effectivenessVariables = new ArrayList<Variable>(effectivenessPotential.getVariables());
    	for (Variable effectivenessVariable : effectivenessVariables) {
    		if (!ceVariables.contains(effectivenessVariable)) {
    			ceVariables.add(effectivenessVariable);
    		}
    	}
    	
    	// Get cost and effectiveness potential
    	GTablePotential gPotential = new GTablePotential(new ArrayList<Variable>(ceVariables), PotentialRole.UTILITY);

    	if (ceVariables.size() == 0) { 
    		// Cost and effectiveness potentials are constants, so also the cost-effectiveness potential
			Intervention[] interventions = new Intervention[1];
			CEP partition = new CEP(
					interventions, costPotential.values, effectivenessPotential.values, null, lambdaMin, lambdaMax);
			gPotential.elementTable.add(partition);    		
    	} else {
    		int[] dimensionsResult = gPotential.getDimensions();

    		// Offsets accumulate algorithm
    		// 1. Set up variables
    		int[] coordinate = new int[dimensionsResult.length];
    		int[][] accumulatedOffsets = new int[2][];
    		accumulatedOffsets[0] = gPotential.getAccumulatedOffsets(costPotential.getVariables());
    		accumulatedOffsets[1] = gPotential.getAccumulatedOffsets(effectivenessPotential.getVariables());
    		int[] positions = {costPotential.getInitialPosition(), effectivenessPotential.getInitialPosition()};
    		int increasedVariable; 
    		int tableSize = gPotential.getTableSize();

    		// 2. Potential initialization operation
    		for (int i = 0; i < tableSize; i++) {
    			double[] costs = {costPotential.values[positions[0]]};
    			double[] effectivities = {effectivenessPotential.values[positions[1]]};
    			Intervention[] interventions = new Intervention[1];
    			CEP partition = new CEP(
    					interventions, costs, effectivities, null, lambdaMin, lambdaMax);
    			gPotential.elementTable.add(partition);
    			// calculate next position using accumulated offsets
    			increasedVariable = 0;
    			for (int j = 0; j < coordinate.length; j++) {
    				coordinate[j]++;
    				if (coordinate[j] < dimensionsResult[j]) {
    					increasedVariable = j;
    					break;
    				}
    				coordinate[j] = 0;
    			}

    			// 3. Update the positions of the potentials we are multiplying
    			for (int j = 0; j < positions.length; j++) {
    				positions[j] += accumulatedOffsets[j][increasedVariable];
    			}   			
    		}
    	}
		
		Variable ceVariable = new Variable(defaultCEVariableName);
		ceVariable.setVariableType(VariableType.NUMERIC);
		gPotential.setUtilityVariable(ceVariable);
   		return gPotential;
    }

	/** Get potentials attached to a variable whose criterion is cost and sums them.
	 * @param probNet. <code>ProbNet</code>
	 * @return <code>TablePotential</code>
	 * @throws CostEffectivenessException   */
	private static TablePotential getCostPotential(ProbNet influenceDiagram) 
			throws CostEffectivenessException {

		List<Variable> variables = influenceDiagram.getVariables();
		ArrayList<TablePotential> costPotentials = new ArrayList<TablePotential>();
		for (Variable variable : variables) {
			if (CEBaseOperations.isCostVariable(variable))	{
				Collection<Potential> potentialsVariable = influenceDiagram.getPotentials(variable);
				for (Potential potential : potentialsVariable) {
					if (potential.getPotentialRole() == PotentialRole.UTILITY) {
						try {
							costPotentials.add(potential.getCPT());
						} catch (NonProjectablePotentialException
								| WrongCriterionException e) {
							throw new CostEffectivenessException(e.getMessage());
						}
					}
				}
			}
		}
		TablePotential costPotential = DiscretePotentialOperations.sum(costPotentials);
		return costPotential;
	}

	/** Get potentials attached to a variable whose criterion is effectiveness and adds them.
	 * @param probNet. <code>ProbNet</code>
	 * @return <code>TablePotential</code>
	 * @throws CostEffectivenessException   */
	private static TablePotential getEffectivenessPotential(ProbNet influenceDiagram) 
			throws CostEffectivenessException {
		List<Variable> variables = influenceDiagram.getVariables();
		ArrayList<TablePotential> effectivenessPotentials = new ArrayList<TablePotential>();
		for (Variable variable : variables) {
			if (CEBaseOperations.isEffectivenessVariable(variable)) {
				Collection<Potential> potentialsVariable = influenceDiagram.getPotentials(variable);
				for (Potential potential : potentialsVariable) {
					if (potential.getPotentialRole() == PotentialRole.UTILITY) {
						try {
							effectivenessPotentials.add(potential.getCPT());
						} catch (NonProjectablePotentialException
								| WrongCriterionException e) {
							throw new CostEffectivenessException(e.getMessage());
						}
					}
				}
			}
		}
		TablePotential effectivenessPotential = DiscretePotentialOperations.sum(effectivenessPotentials);
		return effectivenessPotential;
	}


}
