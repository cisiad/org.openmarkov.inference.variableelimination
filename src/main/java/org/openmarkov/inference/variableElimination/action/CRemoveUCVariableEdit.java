//package org.openmarkov.inference.variableElimination.action;
//
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//import org.openmarkov.core.action.AddLinkEdit;
//import org.openmarkov.core.action.AddPotentialEdit;
//import org.openmarkov.core.action.RemoveLinkEdit;
//import org.openmarkov.core.action.RemoveNodeEdit;
//import org.openmarkov.core.action.RemovePotentialEdit;
//import org.openmarkov.core.action.UsesVariable;
//import org.openmarkov.core.model.network.Node;
//import org.openmarkov.core.model.network.ProbNet;
//import org.openmarkov.core.model.network.Variable;
//import org.openmarkov.core.model.network.potential.Potential;
//import org.openmarkov.core.model.network.potential.PotentialRole;
//import org.openmarkov.core.model.network.potential.TablePotential;
//import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
//import org.openmarkov.inference.variableElimination.operation.VariableEliminationOperations;
//
//@SuppressWarnings("serial")
//public abstract class CRemoveUCVariableEdit extends CRemoveVariableEdit implements UsesVariable {
//
//    private List<Variable> decisionsUntilEnd;
//
//    /**
//     * @param probNet The markov decision network
//     * @param variableToDelete Variable that is to be deleted
//     */
//    public CRemoveUCVariableEdit(
//            ProbNet probNet ,
//            Variable variableToDelete) {
//        super(probNet, variableToDelete);
//    }
//
//    public void undo() {
//        super.undo();
//    }
//
//    @SuppressWarnings("unchecked")
//    public void generateEdits() {
//        Node node = probNet.getNode(variable);
//        List<Node> siblings = node.getSiblings();
//
//        //Get the potentials that depend on the variable
//        List<? extends Potential> probPotentials = probNet.getProbPotentials(variable);
//        List<? extends Potential> utilityPotentials = probNet.getUtilityPotentials(variable);
//
//        //if (useCoalescedInterventions){
////        utilityPotentials = VariableEliminationOperations.orderPotentialsByPartialOrder(
////                (List<TablePotential>) utilityPotentials,decisionsUntilEnd);
//        //}
//
//        List<TablePotential> newPotentials = multiplyAndEliminatePotentials(
//                (List<TablePotential>) probPotentials,
//                (List<TablePotential>) utilityPotentials);
//
//
///*            try {
//                newPotentials.removeConstantPotentials();
//            } catch (IncompatibleEvidenceException e) {
//                e.printStackTrace();
//            }*/
//
//        // add edit to add the new potentials
//        for (Potential newPotential : newPotentials) {
//            if (newPotential != null) {
//                edits.add(new AddPotentialEdit(probNet, newPotential));
//            }
//        }
//
//        // Remove old potentials
//        List<TablePotential>  potentialsContainingVariable = new ArrayList<>((
//                List<TablePotential>) probPotentials);
//        potentialsContainingVariable.addAll((List<TablePotential>) utilityPotentials);
//
//        for (Potential potential : potentialsContainingVariable) {
//            edits.add(new RemovePotentialEdit(probNet, potential));
//        }
//
//        // add a link between every pair of siblings of the removed node
//        for (Node node1 : siblings) {
//            for (Node node2 : siblings) {
//                if (node1 != node2 && !node1.isSibling(node2)) {
//                    addEdit(new AddLinkEdit(probNet,
//                            node1.getVariable(),
//                            node2.getVariable(),
//                            false,
//                            false));
//                }
//            }
//        }
//
//        for (Node sibling : siblings) {
//            Variable siblingVariable = sibling.getVariable();
//            addEdit(new RemoveLinkEdit(probNet, siblingVariable, variable, false, false));
//        }
//
//        // add edit to remove the variable
//        addEdit(new RemoveNodeEdit(probNet, variable));
//    }
//
//    protected List<TablePotential> multiplyAndEliminatePotentials(List<TablePotential> probPotentialsVariable, List<TablePotential> utilityPotentialsVariable) {
//        List<TablePotential> newPotentials = new ArrayList<>();
//
//        List<TablePotential> potentials = new ArrayList<>();
//        potentials.addAll(probPotentialsVariable);
//        potentials.addAll(utilityPotentialsVariable);
//        //List<TablePotential> potentialsAfterElim = DiscretePotentialOperations.sumOutVariable(variable, potentials);
//        List<TablePotential> potentialsAfterElim = marginalizeOutVariable(variable, potentials);
//
//        List<TablePotential> outputProbPotentials = new ArrayList<>();
//        List<TablePotential> outputUtilPotentials = new ArrayList<>();
//        DiscretePotentialOperations.classifyProbAndUtilityPotentials(
//                potentialsAfterElim, outputProbPotentials, outputUtilPotentials);
//
//        TablePotential auxPolicy = extractPolicy(outputProbPotentials);
//
//        if (auxPolicy != null) {
//            outputProbPotentials.remove(auxPolicy);
//            auxPolicy.setPotentialRole(PotentialRole.CONDITIONAL_PROBABILITY);
//            policy = auxPolicy;
//        }
//
//        Set<TablePotential> unityPotentials = findUnityPotentials(outputProbPotentials);
//        for (TablePotential unity:unityPotentials){
//            outputProbPotentials.remove(unity);
//        }
//
///*        newProbPotential = calculateNewPotentialAfterMarginalize(outputProbPotentials));
//       newPotentials.add(newProbPotential);*/
//        newPotentials.addAll(calculateNewPotentialAfterMarginalize(outputProbPotentials));
//
//     /*   newUtilPotential = calculateNewPotentialAfterMarginalize(outputUtilPotentials);
//        newPotentials.add(newUtilPotential);*/
//        newPotentials.addAll(calculateNewPotentialAfterMarginalize(outputUtilPotentials));
//        return newPotentials;
//    }
//
//    private Set<TablePotential> findUnityPotentials(List<TablePotential> outputProbPotentials) {
//
//        Set<TablePotential> unity = new HashSet<>();
//
//        //boolean found = false;
//        /*
//            Condition !found is always 'true' when reached
//            and is never changed inside the for statement
//         */
//        //for (int i=0;i<outputProbPotentials.size()&&!found;i++){
//        for (TablePotential auxPot : outputProbPotentials) {
//            double[] values = auxPot.values;
//            boolean isUnity = true;
//            for (int j = 0; (j < values.length) && isUnity; j++) {
//                isUnity = values[j] == 1.0;
//            }
//            if (isUnity) {
//                unity.add(auxPot);
//            }
//        }
//        return unity;
//    }
//
//    /**
//     * @param outputProbPotentials The new potentials calculated
//     * @return A policy, if any
//     */
//    private TablePotential extractPolicy(List<TablePotential> outputProbPotentials) {
//        TablePotential potPolicy=null;
//        boolean found = false;
//
//        for (int i=0;i<outputProbPotentials.size()&&!found;i++){
//            TablePotential auxPot = outputProbPotentials.get(i);
//            if (auxPot.getPotentialRole()==PotentialRole.POLICY){
//                potPolicy = auxPot;
//                found = true;
//            }
//
//        }
//        return potPolicy;
//    }
//
//    /**
//     * @param outputPotentials The new list of table potentials calculated
//     * @return Those potentials that actually have to be included in the network
//     */
//    private List<TablePotential> calculateNewPotentialAfterMarginalize(
//            List<TablePotential> outputPotentials) {
//        List<TablePotential> returnedPotentials = new ArrayList<>();
//        for (TablePotential outputPotential : outputPotentials) {
//            if (outputPotentials.size()>0) {
//                returnedPotentials.add(outputPotential);
//            }
//        }
//        return returnedPotentials;
//    /*    return ((outputPotentials!=null)&&(outputPotentials.size()>0))?
//                outputPotentials.get(0):null;*/
//    }
//
//    protected abstract List<TablePotential> marginalizeOutVariable(Variable varToMarginalize, List<TablePotential> potentials);
//}