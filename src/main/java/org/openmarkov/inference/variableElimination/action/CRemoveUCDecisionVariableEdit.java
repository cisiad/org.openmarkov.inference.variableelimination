//package org.openmarkov.inference.variableElimination.action;
//
//import java.util.List;
//
//import org.openmarkov.core.model.network.ProbNet;
//import org.openmarkov.core.model.network.Variable;
//import org.openmarkov.core.model.network.potential.TablePotential;
//import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
//
///**
// * Removes a decision node in a variable elimination algorithm.
// * This Edit class is valid for Bayesian networks and influence diagrams.
// * */
//@SuppressWarnings("serial")
//public class CRemoveUCDecisionVariableEdit extends CRemoveUCVariableEdit {
//
//	public CRemoveUCDecisionVariableEdit(ProbNet probNet , Variable variableToDelete) {
//		super(probNet, variableToDelete);
//	}
//
//	@Override
//	protected List<TablePotential> marginalizeOutVariable(
//			Variable varToMarginalize,
//			List<TablePotential> potentials) {
//		return DiscretePotentialOperations.maxOutVariable(varToMarginalize, potentials,false);
//
//	}
//
//}
