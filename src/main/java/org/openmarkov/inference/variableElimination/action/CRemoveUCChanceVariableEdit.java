//package org.openmarkov.inference.variableElimination.action;
//
//import java.util.List;
//
//import org.openmarkov.core.model.network.ProbNet;
//import org.openmarkov.core.model.network.Variable;
//import org.openmarkov.core.model.network.potential.TablePotential;
//import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
//
//@SuppressWarnings("serial")
///** Removes a chance node in a variable elimination algorithm.
// * This Edit class is valid for Bayesian networks and influence diagrams.
// * */
//public class CRemoveUCChanceVariableEdit extends CRemoveUCVariableEdit {
//
//	public CRemoveUCChanceVariableEdit(ProbNet probNet, Variable variableToDelete) {
//		super(probNet, variableToDelete);
//	}
//
//	@Override
//	protected List<TablePotential> marginalizeOutVariable(Variable varToMarginalize, List<TablePotential> potentials) {
//		return DiscretePotentialOperations.sumOutVariable(varToMarginalize, potentials, true);
//	}
//}
